﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ТпЛр2.Models
{
    public interface Ware
    {
        string kategorie { get; set; }
        string name { get; set; }
        int preis { get; set; }
        int nutzbarkeit { get; set; }
    }
}

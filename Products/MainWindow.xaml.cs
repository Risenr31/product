﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using Microsoft.Win32;
using ТпЛр2.Models;
using Products.Kontroller;

namespace Products
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            /*string product;
            string[] odd = new string[4];
            FileStream mybag = new FileStream("E:/Документы/ЯГТУ/ТпЛр2/Продукты.txt", FileMode.Open, FileAccess.Read);
            StreamReader reader = new StreamReader(mybag);
            List<string> Einkaufsliste = new List<string>();
            Menu.ItemsSource = Einkaufsliste;
            while (!reader.EndOfStream)
            {
                product = Convert.ToString(reader.ReadLine());
                odd = product.Split(':');
                Console.WriteLine(odd[1]);
                Einkaufsliste.Add(reader.ReadLine());
            }
            reader.Close();
            mybag.Close();
            Console.WriteLine("Введите сумму,на которую хотите совершить покупку: ");*/
        }
        int[] polesnost;
        int[] zena;
        public List<Ware> Pokupki = new List<Ware>();
        public List<int> BestPokupki = new List<int>();

        public void ChooseFileClick(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog opfidi = new OpenFileDialog();
                opfidi.ShowDialog();
                List<string> Einkaufsliste = new List<string>();
                string productString;
                if (opfidi.FileName != "")
                {
                    productString = File.ReadAllText(opfidi.FileName);
                    Einkaufsliste.AddRange(productString.Split('\n'));
                }
                polesnost = new int[Einkaufsliste.Count];
                zena = new int[Einkaufsliste.Count];
                Pokupki.Clear();
                Menu.ItemsSource = null;
                for (int i = 0; i < Einkaufsliste.Count; i++)
                {
                    string[] odd = new string[4];
                    odd = Einkaufsliste[i].Split(':');
                    Pokupki.Add(item: new Artikel() { kategorie = odd[0], name = odd[1], preis = Convert.ToInt32(odd[2]), nutzbarkeit = Convert.ToInt32(odd[3]) });
                    zena[i] = Convert.ToInt32(odd[2]);
                    polesnost[i] = Convert.ToInt32(odd[3]);
                }
                Menu.ItemsSource = Pokupki;
            }
            catch (Exception)
            {
                MessageBox.Show("Проверьте формат файла и путь");
                Path.Text = "Путь к файлу";
            }
        }

        public void Count_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int i = 0;
                Knapsack.Items.Clear();
                int money = Math.Abs(Convert.ToInt32(Summ.Text));
                BestPokupki = Knapsack_problem.knapsack(zena, polesnost, money);
                for (i = 0; i < (BestPokupki.Count - 1); i++)
                    Knapsack.Items.Add(Pokupki[BestPokupki[i]].name);
                Knapsack.Items.Add("Итоговая полезность: " + BestPokupki[i]);
                //Knapsack.ItemsSource = BestPokupki;
            }
            catch (Exception)
            { 
                MessageBox.Show("Введите корректно сумму денег");
                Summ.Text = "Сумма денег";
            }
        }
    }
}
